
function Set-ADUserPass {
	[CmdletBinding()]
    param (
        [Parameter(Mandatory)][String]$Name,
		[Parameter(Mandatory)][SecureString]$Password,
		[Parameter(Mandatory)][String]$ADServer,
        [ValidateNotNull()]
        [System.Management.Automation.PSCredential]
        [System.Management.Automation.Credential()]
        $Credentials = [System.Management.Automation.PSCredential]::Empty,
		[Parameter()][String]$OU = "$(Get-ADDomain).DistinguishedName"
		)
	$user = Get-ADUser -Filter "displayName -eq '$Name'" -Property ObjectGUID -SearchBase $OU -Server $ADServer -Credential $Credentials
	# Set password
	Set-ADAccountPassword -Identity $user.ObjectGUID -NewPassword $Password -Reset -Server $ADServer -Credential $Credentials -Verbose
	if ($?) {$SetADAccountPassword = "OK"}
	# Set "change at logon" flags
	Set-ADUser -Identity $user.ObjectGUID -ChangePasswordAtLogon:$true -Server $ADServer -Credential $Credentials -Verbose
	if ($?) {$SetADUser = "OK"}
	# Set "password expired" flags
	Set-ADObject -Identity $user.ObjectGUID -Replace:@{"userAccountControl"="8389120"} -Server $ADServer -Credential $Credentials -Verbose
	if ($?) {$SetADObject = "OK"}
	# Enable account
	Enable-ADAccount -Identity $user.ObjectGUID -Server $ADServer -Credential $Credentials -Verbose
	if ($?) {$EnableADAccount = "OK"}
	$output = [PSCustomObject]@{
		Name = $Name
		SetADAccountPassword = $SetADAccountPassword
		SetADUser = $SetADUser
		SetADObject = $SetADObject
		EnableADAccount = $EnableADAccount
	}
	Write-Output $output
}

$users = Read-Host "List of users (Display Name) separated by colon (;)"
$password = (Read-Host -Prompt "Specify a desired new password" -AsSecureString) 
$ADServer = Read-Host "Specify AD server name"
$Cred = Get-Credential -Message "Enter credentials. Use UPN as user name, like 'user@domain.com'"

$export = @()
foreach ($name in $users.split(';')) {
	$export += Set-ADUserPass -Name $name -Password $password -ADServer $ADServer -Credential $Cred
}
$export | Export-Csv -Path c:\temp\Set-ADUserPass.csv -NoTypeInformation -Append -Encoding "Unicode"
$export.Count
