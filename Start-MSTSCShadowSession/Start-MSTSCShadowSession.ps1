﻿$pcs = Read-Host "Enter PC name/IP (or multiple separated by comma) or enter Q to quit"
do
    {
    $pcs = $pcs.Split(',')
    foreach ($pc in $pcs)
        {
        "***" + $pc + "***"
        Clear-DnsClientCache
        Resolve-DnsName $pc
        if (Test-Connection -ComputerName $pc -Count 1 -Quiet -ErrorAction SilentlyContinue) 
            {
            "Host online. Querying sessions..."
            qwinsta /server:$pc
            $session = Read-Host "Enter session number or enter Q to cancel"
            if (($session -ne "Q") -or ($session -ne "q"))
                {
                Mstsc /shadow:$session /v:$pc /control /admin
                }
            }
        else {"Host unreachable"}
        }
    $pcs = Read-Host "Enter PC name (or multiple separated by comma) or enter Q to quit"
    }
while (($pcs -ne "Q") -or ($pcs -ne "q"))
