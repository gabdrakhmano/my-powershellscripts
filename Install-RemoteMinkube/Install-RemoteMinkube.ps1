$Cred = Get-Credential
$pc = Read-Host "Hostname"
$Session = New-PSSession -ComputerName $pc -Credential $Cred
Invoke-Command -Session $Session -FilePath $PSScriptRoot\enable-HyperV\enable-hyperv.ps1
Restart-Computer -ComputerName $pc -Credential $Cred -Force -Wait -For PowerShell -Timeout 900 -Delay 2
$Session = New-PSSession -ComputerName $pc -Credential $Cred
Invoke-Command -Session $Session -FilePath $PSScriptRoot\install-MinikubeHyperv\create-hypervswitch.ps1
Invoke-Command -Session $Session -FilePath $PSScriptRoot\install-Choco\install-Choco.ps1
Invoke-Command -Session $Session -FilePath $PSScriptRoot\install-MinikubeHyperv\install-minikube.ps1