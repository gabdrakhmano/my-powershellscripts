$netad = $null
Get-NetAdapter | Select-Object -Property Name, Status
$netadlist = Get-NetAdapter | ForEach-Object Name
while ($netad -notin $netadlist) {
    $netad = Read-Host "Enter network adapter name or [cancel] to abort"
    if ($netad -eq 'cancel'){
        break
    }
}
New-VMSwitch -SwitchName "Minikube Virtual Switch" -NetAdapterName "$netad" -AllowManagementOS $true
Get-VMSwitch | Select-Object -Property Name, SwitchType, AllowManagementOS