choco install minikube -y
minikube delete -all
minikube start --vm-driver hyperv --hyperv-virtual-switch "Minikube Virtual Switch"
minikube ip