"Changing sleep delay on AC power..."
powercfg.exe /change standby-timeout-ac 0
powercfg.exe /change hibernate-timeout-ac 0
"Done"

"Setting time zone..."
Set-TimeZone -Id "Russian Standard Time"
"Done"

"Setting language for non-Unicode programs..."
Set-WinSystemLocale ru-RU
"Done"

"Installing packages..."
Start-Process -PassThru -Wait $PSScriptRoot\setuplanguagepack.x86.ru-ru_.exe
Start-Process -PassThru -Wait $PSScriptRoot\system_update_5.07.0106.exe /silent