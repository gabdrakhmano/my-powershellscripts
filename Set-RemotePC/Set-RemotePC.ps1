﻿$scriptpath = $MyInvocation.MyCommand.Path
$dir = Split-Path $scriptpath
Set-Location $dir
while ($true) {
    $pcs = Read-Host "Enter PC name (or multiple separated by comma) or Ctrl+C to quit"
    Clear-DnsClientCache
    $pcs = $pcs.Split(',')
    foreach ($pc in $pcs) {
        "***" + $pc + "***"
        if (Test-Connection -ComputerName $pc -Count 1 -Quiet -ErrorAction SilentlyContinue) { 
            "Copying script..."
            robocopy ".\payload" "\\$pc\c$\temp\$($MyInvocation.MyCommand.Name)" /E /Z /ZB /R:5 /W:5 /TBD /V
            "Done"

            "Executing..."
            .\bin\PsExec.exe /nobanner \\$pc powershell -executionpolicy bypass -windowstyle hidden -noninteractive -nologo -inputformat none -File "c:\temp\$($MyInvocation.MyCommand.Name)\!deploy.ps1" 
            "Done"

            #"Removing script and packages..."
            #Remove-Item -Path "\\$pc\c$\temp\$($MyInvocation.MyCommand.Name)" -Recurse -Force
            #"Done"
        } else {
            "Host unreachable"
        }
    }
}