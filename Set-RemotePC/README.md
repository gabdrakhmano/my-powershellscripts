# Set-RemotePC

## What it does

*Set-RemotePC.ps1* copies content of **payload** folder with robocopy to remote PC you specify (in particular to *C:\temp\Set-RemotePC.ps1\\*) and then uses [PsExec](https://docs.microsoft.com/en-us/sysinternals/downloads/psexec) (which is also included here in **bin**) to execute *!deploy.ps1* on this remote PC from the folder it was copied to. The *!deploy.ps1* itself should be modified beforehand to contain any set of PowerShell scripts you want to be executed. *!deploy.ps1* included here is just an example which can give you an idea of the usage. It does the following:

```powershell
"Changing sleep delay on AC power..."
powercfg.exe /change standby-timeout-ac 0
powercfg.exe /change hibernate-timeout-ac 0
"Done"

"Setting time zone..."
Set-TimeZone -Id "Russian Standard Time"
"Done"

"Setting language for non-Unicode programs..."
Set-WinSystemLocale ru-RU
"Done"

"Installing packages..."
Start-Process -PassThru -Wait $PSScriptRoot\setuplanguagepack.x86.ru-ru_.exe
Start-Process -PassThru -Wait $PSScriptRoot\system_update_5.07.0106.exe /silent
```

## How to use

1. Use **Download** button (located to the left from blue **Clone** button) to download zip archive
1. Extract somewhere on your PC
1. Keep, remove or replace .exe files (in **payload** folder) according to your need
1. Modify *!deploy.ps1* (in **payload** folder) according to your need and content of **payload** folder
1. Execute *Set-RemotePC.bat* as user having elevated privileges on remote PC (i.e. **as domain administrator**)
1. Enter hostname of remote PC (or several separated by comma)
1. Watch the progress

To interrupt the execution use Ctrl+C.

## Requirements

In most cases no configuration is required for Set-RemotePC to work. Still, requirements are

* account having elevated privileges on remote PC (for example domain admin)
* both computers are running Windows 10
* robocopy is present on your computer (**by default**)
* remote computer has ADMIN$ administrative share (**by default**)
* remote computer has File and Printer Sharing allowed in firewall (TCP port 445) (**by default**)

Though organization's security may restrict usage of PsExec for security reasons, **it is not restricted in out-of-the-box Windows 10**.

## Notes

* **payload** folder posted here includes .exe files mentioned in *!deploy.ps1*, but just for the sake of demonstration. This files can be removed or replaced, just be sure to modify *!deploy.ps1* accordingly (change file names in lines under "Installing packages...", add additional ones or remove them).
* Obviously for installation to work it should not wait for user input and be executed in non-interactive / silent mode (for example note */silent* flag after **system_update_5.07.0106.exe**)
* Since Start-Process cmdlet in example *!deploy.ps1* provided here includes *-Wait* flag, it waits for process to finish before going to next one. This allows you to see what is going on and check if installation of specific package is finished or not, but may seem slow.
  * Actually all commands in example *!deploy.ps1* provided here are executed one after another.
* Generally Set-RemotePC better not be used to install heavy apps since it relies on network connection to transfer application packages from local PC to remote PC.

## Things to read about [PsExec](https://docs.microsoft.com/en-us/sysinternals/downloads/psexec)

* https://adamtheautomator.com/psexec/
* https://www.itprotoday.com/windows-server/psexec-explainer-mark-russinovich
