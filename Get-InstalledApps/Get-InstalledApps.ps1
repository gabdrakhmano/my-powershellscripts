$file = "$env:USERPROFILE" + "\" + "$($(Get-CimInstance -ClassName Win32_ComputerSystem).Name)" + "_software.csv"
$file1 = "$env:USERPROFILE" + "\" + "$($(Get-CimInstance -ClassName Win32_ComputerSystem).Name)" + "_apps.csv"

$paths=@(
  'HKLM:\SOFTWARE\WOW6432Node\Microsoft\Windows\CurrentVersion\Uninstall\',
  'HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\'
)
foreach($path in $paths){
  $softwarelist += Get-ChildItem -Path $path | 
    Get-ItemProperty | 
      Select DisplayName, Publisher, InstallDate, DisplayVersion
}
$softwarelist | Sort-Object displayname | Get-Unique -AsString | Export-Csv -Path $file -NoTypeInformation -Force -Encoding UTF8

$appslist = Get-AppxPackage | Select Name, PackageFullName, Version | sort name
$appslist | Export-Csv -Path $file1 -NoTypeInformation -Force -Encoding UTF8

Write-Host "Done. Data saved to $file and $file1" -ForegroundColor Yellow

$address = Read-Host "Enter email address or Ctrl+C to quit"
$Outlook = New-Object -comObject Outlook.Application
$Email = $Outlook.CreateItem(0)
$Email.To = $address
$Email.Subject = "Software // " + $(Get-CimInstance -ClassName Win32_ComputerSystem).name
$Email.Body = "Hostname : " + $(Get-CimInstance -ClassName Win32_ComputerSystem).Name + "`nUserName : " + $(Get-CimInstance -ClassName Win32_ComputerSystem).UserName
$Email.Attachments.Add($file) | Out-Null
$Email.Attachments.Add($file1) | Out-Null
$Email.Send()

Write-Host "Done. Data sent to $address" -ForegroundColor Yellow
