# Set-BitLockerBackupKey

## What it does

*Set-BitLockerBackupKey* checks for BitLocker encrypted and unlocked drives, creates Recovery Password and TPM protectors (if not present) for each and saves recovery password to Active Directory.

## How to use

1. Use **Download** button (located to the left from blue **Clone** button) to download zip archive (under **Download this directory**)
1. Extract
1. Run BAT file as administrator locally (on target PC) or execute PS1 file on remotely using PS remoting or [PsExec](https://docs.microsoft.com/en-us/sysinternals/downloads/psexec) (still under account having admin rights on remote PC)

To interrupt the execution use Ctrl+C.

## Requirements

In most cases no configuration is required. Still, requirements are

* account having elevated privileges on remote PC (for example domain admin)
* computers are running Windows 10

## Notes

* at the moment script always saves recovery password to AD, even if it is already there (though this won't harm in any way, it just creates additional password record in AD)

## Things to read

* https://docs.microsoft.com/en-us/powershell/module/bitlocker/?view=windowsserver2019-ps
