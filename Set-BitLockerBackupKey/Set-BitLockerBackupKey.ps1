do {
	$BLVlist = Get-BitLockerVolume
	foreach ($BLV in $BLVlist) {
		$BLV | Select-Object -Property *
		$mntpoint = $BLV | Select-Object -ExpandProperty MountPoint
		$lockstatus = $BLV | Select-Object -ExpandProperty LockStatus
		$volumestatus = $BLV | Select-Object -ExpandProperty VolumeStatus
		if ($lockstatus -eq "Locked") {
			Write-Host "Volume is locked and will be skipped." -ForegroundColor red
		}
		#comment this lines to include fully decrypted disks
		elseif ($volumestatus -eq "FullyDecrypted") {
			Write-Host "Volume is not encrypted and will be skipped." -ForegroundColor red
		} else {
			Write-Host "Checking presence of Recovery Password protector" -ForegroundColor green
			$protectorID = (@($BLV.KeyProtector | Where-Object { $_.KeyProtectorType -eq "RecoveryPassword" }) | Select-Object KeyProtectorId).KeyProtectorId
			if (-not $protectorID ) {
				Write-Host "There is no Recovery Password protector. Creating..." -ForegroundColor red
				Add-BitLockerKeyProtector $mntpoint -RecoveryPasswordProtector
			}
			$protectorIDTPM = (@($BLV.KeyProtector | Where-Object { $_.KeyProtectorType -eq "TPM" }) | Select-Object KeyProtectorId).KeyProtectorId
			if (-not $protectorIDTPM ) 
			{
				Write-Host "There is no TPM protector. Creating..." -ForegroundColor red
				Add-BitLockerKeyProtector $mntpoint -TPMProtector
			}
			Write-Host "Confirming..." -ForegroundColor green
			$BLV_check = Get-BitLockerVolume -MountPoint $mntpoint
			$protectorID = (@($BLV_check.KeyProtector | Where-Object { $_.KeyProtectorType -eq "RecoveryPassword" }) | Select-Object KeyProtectorId).KeyProtectorId
			$protectorIDTPM = (@($BLV_check.KeyProtector | Where-Object { $_.KeyProtectorType -eq "TPM" }) | Select-Object KeyProtectorId).KeyProtectorId
			$protstatus = $BLV_check | Select-Object -ExpandProperty ProtectionStatus
			if (-not $protectorID) {
				Write-Host "Failed to create Recovery Password protector. Unable to proceed." -ForegroundColor red
			} elseif (-not $protectorIDTPM) {
				Write-Host "Failed to create TPM protector. Unable to proceed." -ForegroundColor red
			} else {
				Write-Host "Saving Recovery Password to AD..." -ForegroundColor green
				Write-Host ""
				Backup-BitLockerKeyProtector -MountPoint $mntpoint -KeyProtectorId $protectorID
				if ($protstatus -eq "Off") {
					Write-Host "Protection is Off. Trying to resume protection..." -ForegroundColor red
					Write-Host ""
					Resume-BitLocker -MountPoint $mntpoint
				}
			}
		}
		Write-Host ""
		Write-Host "=====================================================" -ForegroundColor green
		Write-Host ""
	}
	$goon = Read-Host "`nEnter any key to repeat or Q to quit"
}
while (($goon -ne "Q") -or ($goon -ne "q"))