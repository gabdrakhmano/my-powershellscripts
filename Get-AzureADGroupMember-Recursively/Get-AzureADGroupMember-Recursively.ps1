function Get-AzureADGroupMember-Recursively {
	[CmdletBinding()]
    param (
        [Parameter(Mandatory)][string]$Value,
		[Parameter()][ValidateSet('Mail','DisplayName')][string]$Type = 'DisplayName'
		)
    $group = Get-AzureADGroup -Filter "$Type eq '$Value'"
	$members_users = Get-AzureADGroupMember -ObjectId $group.ObjectId | Where-Object ObjectType -ne "Group" | Select-Object -Property DisplayName, ObjectType, UserPrincipalName, Mail, @{label="MemberOf";expression={$group.Mail}}
	$members_groups = Get-AzureADGroupMember -ObjectId $group.ObjectId | Where-Object ObjectType -eq "Group" | Select-Object -Property DisplayName, ObjectType, UserPrincipalName, Mail, @{label="MemberOf";expression={$group.Mail}}
	foreach ($item in $members_groups) {$members_users += Get-AzureADGroupMember-Recursively $($item.DisplayName)}
	Write-Output $members_users
}


$type = Read-Host "Enter AzureAD group property (Mail or DisplayName)"
$string = Read-Host "Enter list of values for selected property (separated by semicolon)"
$string = $string.Replace(' ','')
$list = $string.Split(";")
$list = $list | Select-Object -Unique


$users = @()
foreach ($item in $list) {
	$users += Get-AzureADGroupMember-Recursively "$item" "$type"
}
$users | Export-Csv -Path c:\temp\Get-AzureADGroupMember-Recursively.csv -NoTypeInformation -Append -Encoding "Unicode"
$users.Count
