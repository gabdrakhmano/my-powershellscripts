$exec_path = "\winpe_pwsh\executor.ps1"
$drives = Get-CimInstance -ClassName win32_logicaldisk
$drives.deviceID | ForEach-Object {
	if (Test-Path "$_$exec_path" -PathType leaf) {
		Write-Host "Starting $_$exec_path..." -ForegroundColor Yellow
		powershell -file "$_$exec_path"
	}
}
