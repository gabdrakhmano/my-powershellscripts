$dir = "$env:SystemDrive\WinPE_amd64_PS"
$proceed = "no"
$answers = "create", "skip"
while ($proceed -notin $answers) {
	Write-Host "Create WinPE image at $dir (ALL EXISTING DATA IN THIS FOLDER WILL BE LOST)? Type [create] to confirm or [skip] to skip : " -ForegroundColor Yellow -NoNewline
	$proceed = Read-Host
}

if ($proceed -eq 'create') {
	Dism /Unmount-Image /MountDir:$dir\mount /discard
	rmdir $dir
	copype amd64 $dir
	Dism /Mount-Image /ImageFile:"$dir\media\sources\boot.wim" /Index:1 /MountDir:"$dir\mount"
	Dism /Add-Package /Image:"$dir\mount" /PackagePath:"$env:SystemDrive\Program Files (x86)\Windows Kits\10\Assessment and Deployment Kit\Windows Preinstallation Environment\amd64\WinPE_OCs\WinPE-WMI.cab"
	Dism /Add-Package /Image:"$dir\mount" /PackagePath:"$env:SystemDrive\Program Files (x86)\Windows Kits\10\Assessment and Deployment Kit\Windows Preinstallation Environment\amd64\WinPE_OCs\en-us\WinPE-WMI_en-us.cab"
	Dism /Add-Package /Image:"$dir\mount" /PackagePath:"$env:SystemDrive\Program Files (x86)\Windows Kits\10\Assessment and Deployment Kit\Windows Preinstallation Environment\amd64\WinPE_OCs\WinPE-NetFX.cab"
	Dism /Add-Package /Image:"$dir\mount" /PackagePath:"$env:SystemDrive\Program Files (x86)\Windows Kits\10\Assessment and Deployment Kit\Windows Preinstallation Environment\amd64\WinPE_OCs\en-us\WinPE-NetFX_en-us.cab"
	Dism /Add-Package /Image:"$dir\mount" /PackagePath:"$env:SystemDrive\Program Files (x86)\Windows Kits\10\Assessment and Deployment Kit\Windows Preinstallation Environment\amd64\WinPE_OCs\WinPE-Scripting.cab"
	Dism /Add-Package /Image:"$dir\mount" /PackagePath:"$env:SystemDrive\Program Files (x86)\Windows Kits\10\Assessment and Deployment Kit\Windows Preinstallation Environment\amd64\WinPE_OCs\en-us\WinPE-Scripting_en-us.cab"
	Dism /Add-Package /Image:"$dir\mount" /PackagePath:"$env:SystemDrive\Program Files (x86)\Windows Kits\10\Assessment and Deployment Kit\Windows Preinstallation Environment\amd64\WinPE_OCs\WinPE-PowerShell.cab"
	Dism /Add-Package /Image:"$dir\mount" /PackagePath:"$env:SystemDrive\Program Files (x86)\Windows Kits\10\Assessment and Deployment Kit\Windows Preinstallation Environment\amd64\WinPE_OCs\en-us\WinPE-PowerShell_en-us.cab"
	Dism /Add-Package /Image:"$dir\mount" /PackagePath:"$env:SystemDrive\Program Files (x86)\Windows Kits\10\Assessment and Deployment Kit\Windows Preinstallation Environment\amd64\WinPE_OCs\WinPE-StorageWMI.cab"
	Dism /Add-Package /Image:"$dir\mount" /PackagePath:"$env:SystemDrive\Program Files (x86)\Windows Kits\10\Assessment and Deployment Kit\Windows Preinstallation Environment\amd64\WinPE_OCs\en-us\WinPE-StorageWMI_en-us.cab"
	Dism /Add-Package /Image:"$dir\mount" /PackagePath:"$env:SystemDrive\Program Files (x86)\Windows Kits\10\Assessment and Deployment Kit\Windows Preinstallation Environment\amd64\WinPE_OCs\WinPE-DismCmdlets.cab"
	Dism /Add-Package /Image:"$dir\mount" /PackagePath:"$env:SystemDrive\Program Files (x86)\Windows Kits\10\Assessment and Deployment Kit\Windows Preinstallation Environment\amd64\WinPE_OCs\en-us\WinPE-DismCmdlets_en-us.cab"
	Dism /Unmount-Image /MountDir:$dir\mount /Commit
}
elseif  ($proceed -eq 'skip') {
	Write-Host "Skipping WinPE image creation" -ForegroundColor Yellow
	}

$proceed = "no"
$answers = "add", "skip"
while ($proceed -notin $answers) {
	Write-Host "Add winpeshl.ini and runner.ps1 (from $PSScriptRoot) to the image (OVERWRITES)? Type [add] to confirm or [skip] to skip : " -ForegroundColor Yellow -NoNewline
	$proceed = Read-Host
}

if ($proceed -eq 'add') {
	Dism /Mount-Image /ImageFile:"$dir\media\sources\boot.wim" /Index:1 /MountDir:"$dir\mount"
	copy "$PSScriptRoot\winpeshl.ini" "$dir\mount\Windows\System32"
	copy "$PSScriptRoot\runner.ps1" "$dir\mount"
	Dism /Unmount-Image /MountDir:$dir\mount /Commit
}
elseif  ($proceed -eq 'skip') {
	Write-Host "Skipping winpeshl.ini and runner.ps1 copy" -ForegroundColor Yellow
	}

Get-ChildItem '$dir' | Format-Table -AutoSize

$proceed = "no"
$answers = "write", "quit"
while ($proceed -notin $answers) {
	Write-Host "Write WinPE image from '$dir' to USB (ALL EXISTING DATA ON $usb WILL BE LOST)? Type [write] to confirm or [quit] to quit : " -ForegroundColor Yellow -NoNewline
	$proceed = Read-Host
}
	
if ($proceed -eq 'write') {
	$usb = "undefined"
	$drives = Get-CimInstance -ClassName win32_logicaldisk
	while ($usb -notin $drives.deviceID) {
		$drives  | Format-Table -AutoSize
		Write-Host "Enter USB drive DeviceID followed by colon (e.g. D:) to write WinPE on: " -ForegroundColor Yellow -NoNewline
		$usb = Read-Host
	}
	MakeWinPEMedia /UFD $dir $usb
	mkdir $usb\winpe_pwsh
	copy "$PSScriptRoot\executor.ps1" "$usb\winpe_pwsh"
}
elseif  ($proceed -eq 'quit') {
	Write-Host "Quiting without writing to USB" -ForegroundColor Yellow
	Break
}