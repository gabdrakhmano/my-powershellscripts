* Download New-WinPEPwshUSB and all files

* Modify executor.ps1 content according to your need (it is a script which runs on WinPE boot) or leave empty

* Install the Windows Assessment and Deployment Kit (ADK) and Windows PE add-on for ADK, adding the Deployment Tools and Windows Preinstallation Environment features. 

	- Download and install the Windows ADK for Windows 10
	- Download and install the Windows PE add-on for the ADK
	- (https://docs.microsoft.com/en-us/windows-hardware/get-started/adk-install)

* Start the Deployment and Imaging Tools Environment as an administrator.

* Change directory to the one where you saved the New-WinPEPwshUSB.ps1 and other content:

	cd "C:\PathTo\FolderWith\New-WinPEPwshUSB"

* Then type in the following command to create WinPE image with Powershell and write to USB:

	powershell -file .\New-WinPEPwshUSB.ps1
	***to cancell the process at any point press Ctrl + C

* You will get a bootable WinPE USB which autastart Powershell on boot and runs executor.ps1 script

	example executor.ps1 script: script which reads hardware specification and writes it to file