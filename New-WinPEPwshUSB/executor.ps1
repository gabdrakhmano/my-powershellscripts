foreach( $i in 1..100){
    [system.enum]::GetValues('consolecolor') | ForEach-Object{ 
        Write-Host "DISPLAYCOLORSTEST" -ForegroundColor $_ -BackgroundColor $_ -NoNewline
    }
}

$file = "$PSScriptRoot\pcspec.csv"
$objects = @(
    @{
        name        =   "Win32_ComputerSystemProduct"
        properties  =   "IdentifyingNumber", "Vendor", "Name"
    },
    @{
        name        =   "Win32_Processor"
        properties  =   "Name"
    },
    @{
        name        =   "Win32_PhysicalMemory"
        properties  =   "Manufacturer", "PartNumber", "Capacity", "Speed", "DeviceLocator"
    },
    @{
        name        =   "Win32_DiskDrive"
        properties  =   "Name", "Model", "Size", "InterfaceType", "MediaType"
    }
    @{
        name        =   "Win32_VideoController"
        properties  =   "Name", "AdapterRAM"
    }
)

$myObject = [PSCustomObject]@{}
foreach( $object in $objects )
{
    foreach( $property in $object.properties )
    {
        $myObject | Add-Member -MemberType NoteProperty -Name "$($object.name).$property" -Value (
            ($(Get-CimInstance -ClassName $object.name | Where-Object {$_.InterfaceType -ne "USB"}).$property) -join '; '
        )
    }
}
$myObject | Export-Csv -Path $file -NoTypeInformation -Append -Force
$myObject

Write-Host "Done. System specs saved to $file" -ForegroundColor Yellow