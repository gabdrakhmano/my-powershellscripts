$pcs = Read-Host "Enter PC name/IP or [Q] to quit"
$remotepath = "C:\Temp"
do
    {
    $pcs = $pcs.Split(',')
    foreach ($pc in $pcs)
        {
        "***" + $pc + "***"
        Clear-DnsClientCache
        Resolve-DnsName $pc
        if (Test-Connection -ComputerName $pc -Count 1 -Quiet -ErrorAction SilentlyContinue) 
            {
            "Host online. Querying sessions..."
            qwinsta /server:$pc
            psexec /nobanner -accepteula \\$pc powershell
            }
        else {"Host unreachable"}
        }
    $pcs = Read-Host "Enter PC name/IP or [Q] to quit"
    }
while (($pcs -ne "Q") -or ($pcs -ne "q"))