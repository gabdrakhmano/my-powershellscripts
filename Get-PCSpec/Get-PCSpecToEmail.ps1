$file = "$env:USERPROFILE" + "\" + "$($(Get-CimInstance -ClassName Win32_ComputerSystem).Name)" + "_software.csv"
$objects = @(
    @{
        name        =   "Win32_ComputerSystemProduct"
        properties  =   "IdentifyingNumber", "Vendor", "Name"
    },
    @{
        name        =   "Win32_Processor"
        properties  =   "Name"
    },
    @{
        name        =   "Win32_PhysicalMemory"
        properties  =   "Manufacturer", "PartNumber", "Capacity", "Speed", "DeviceLocator"
    },
    @{
        name        =   "Win32_DiskDrive"
        properties  =   "Name", "Model", "Size", "InterfaceType", "MediaType"
    }
    @{
        name        =   "Win32_VideoController"
        properties  =   "Name", "AdapterRAM"
    }
)

$myObject = [PSCustomObject]@{}
foreach( $object in $objects )
{
    foreach( $property in $object.properties )
    {
        $myObject | Add-Member -MemberType NoteProperty -Name "$($object.name).$property" -Value (
            ($(Get-CimInstance -ClassName $object.name | Where-Object {$_.InterfaceType -ne "USB"}).$property) -join '; '
        )
    }
}
$myObject | Export-Csv -Path $file -NoTypeInformation -Append -Force
Write-Host $myObject

Write-Host "Done. Data saved to $file" -ForegroundColor Yellow

$address = Read-Host "Enter email address or Ctrl+C to quit"
$Outlook = New-Object -comObject Outlook.Application
$Email = $Outlook.CreateItem(0)
$Email.To = $address
$Email.Subject = "PCSPEC // " + $(Get-CimInstance -ClassName Win32_ComputerSystem).name
$Email.Body = "Hostname : " + $(Get-CimInstance -ClassName Win32_ComputerSystem).Name + "`nUserName : " + $(Get-CimInstance -ClassName Win32_ComputerSystem).UserName
$Email.Attachments.Add($file) | Out-Null
$Email.Send()

Write-Host "Done. Data sent to $address" -ForegroundColor Yellow
