Write-Host "Enter PC name (or multiple separated by comma) or enter Q to quit: " -ForegroundColor Yellow -NoNewline
$pcs = Read-Host
do {
    $pcs = $pcs.Split(',')
    foreach ($pc in $pcs) {
        "======== " + $pc + " ========"
        Clear-DnsClientCache
        Resolve-DnsName $pc
        if (Test-Connection -ComputerName $pc -Count 1 -Quiet -ErrorAction SilentlyContinue) {
            Write-Host "Host online. Querying sessions..." -ForegroundColor Yellow
            qwinsta /server:$pc
            Write-Host "Type [yes] to proceed with enabling PowerShell remoting or anything else to cancel: " -ForegroundColor Yellow -NoNewline
            $session = Read-Host
            if (($session -eq "yes")) {
                .\PsExec.exe /nobanner \\$pc -s powershell Enable-PSRemoting -Force
            }
        }
        else {
            Write-Host "Host unreachable" -ForegroundColor Yellow
        }
    }
    Write-Host "Enter PC name (or multiple separated by comma) or enter Q to quit: " -ForegroundColor Yellow -NoNewline     
    $pcs = Read-Host 
}
while (($pcs -ne "Q") -or ($pcs -ne "q"))